# Semantic release for GitLab

Automate version management.

[go-semrel-gitlab](https://gitlab.com/juhani/go-semrel-gitlab) is a set of commands
you can use to compose your automated release workflow.

- it's designed to run in [GitLab](https://gitlab.com/) CI pipeline
- it's available as a docker image or a single binary, so it's easy to include in your pipeline

The executable name is **`release`**, and it can

- determine the next version from the previous version and commit messages
- create/update changelog
- create tags with release notes
- attach files to release notes
- commit version bumps (eg. `package.json, pom.xml, CHANGELOG.md` ...)

This project was inspired by [semantic-release](https://github.com/semantic-release).
You should read it's excellent [documentation](https://semantic-release.gitbooks.io/semantic-release)
to learn about the principles and benefits of release automation. Pay special attention to 
[commit message format](https://semantic-release.gitbooks.io/semantic-release/content/#commit-message-format).
[go-semrel-gitlab](https://gitlab.com/juhani/go-semrel-gitlab) depends on that, too.

### Comparison to [semantic-release](https://github.com/semantic-release)

The key difference of `go-semrel-gitlab` and `semantic-release` is that they approach the problem from different angles:

- `semantic-release` has a pre-defined, configurable
  [workflow](https://semantic-release.gitbooks.io/semantic-release/#release-steps) which was originally
  designed for publishing npm packages from Github, but now there are
  [plugins](https://semantic-release.gitbooks.io/semantic-release/content/docs/extending/plugins-list.html)
  for GitLab, DockerHub, and more.
- `go-semrel-gitlab` leaves workflow coordination to Gitlab CI, provides few (hopefully simple) commands
  to automate the versioning and gitlab related tasks of a release, and tries to minimize the constraints
  it imposes to the pipeline configuration.

## More information

- [Howtos](https://juhani.gitlab.io/go-semrel-gitlab/howto)
- [CLI usage](https://juhani.gitlab.io/go-semrel-gitlab/cli/v0.18.0/help/)

## TODO

- more documentation
- configurable release notes
