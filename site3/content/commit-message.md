
---
title: "Format commit messages"
description: "go-semrel-gitlab depends on the information you put in commit messages"
weight: 31
---

# Formatting commit messages

`go-semrel-gitlab` extracts information from commit messages to determine the next version bump
and to produce release notes.

## Message header

The first line of commit messages should be like

```
type(scope): subject
```

or 

```
type: subject
```

- type is used to determine if the commit should cause a patch or minor bump
- scope and subject are included in the release note if the type indicates a version bump 
- default types in go-semrel-gitlab are
  - major bump: `feat`
  - patch bump: `fix,refactor,perf,docs,style,test`
- you can customize the types by assinging your own type lists to environment variables
  `GSG_PATCH_COMMIT_TYPES` and/or `GSG_MINOR_COMMIT_TYPES`

## Breaking changes

When a commit contains a breaking change, the commit message should contain `BREAKING CHANGE:` to trigger a major version bump.

For example

```
refactor: unify flag naming
    
BREAKING CHANGE: Flags corresponding with CI_ env-vars are prefixed with 'ci-'.

- project-path -> ci-project-path
- project-url -> ci-project-url
- commit-sha -> ci-commit-sha
- commit-tag -> ci-commit-tag
- commit-ref-name -> ci-commit-ref-name
```

- `BREAKING CHANGE:` overrides the type information in the first line.
- The text after `BREAKING CHANGE:` is included in the release note.

## Examples

### Fixes

Commit message

```
refactor(actions): switch to using actions api
```

will appear in release note in Fixes section like this

> - **actions:** switch to using actions api ([65d00257](https://gitlab.com/juhani/go-semrel-gitlab/commit/65d00257d5b1a6eb310ddb7fb0b8b48ade7b40a3))

### Features

Commit message

```
feat(next-version): add flag to return curr-ver if no changes detected
    
Fixes #38
```

will appear in release note in Features section like this

> - **next-version:** add flag to return curr-ver if no changes detected ([80895de9](https://gitlab.com/juhani/go-semrel-gitlab/commit/80895de90141d57f34f4320eab8fa54bb253a59e))

### Breaking changes

Commit message

```
refactor: unify flag naming
    
BREAKING CHANGE: Flags corresponding with CI_ env-vars are prefixed with 'ci-'.

- project-path -> ci-project-path
- project-url -> ci-project-url
- commit-sha -> ci-commit-sha
- commit-tag -> ci-commit-tag
- commit-ref-name -> ci-commit-ref-name
```

will appear in Breaking changes section like this

>
> ### unify flag naming ([69485bc0](https://gitlab.com/juhani/go-semrel-gitlab/commit/69485bc01afd4fff018cd2d2b1de7efc0e36b0a7))
> 
> Flags corresponding with CI_ env-vars are prefixed with 'ci-'.
> 
> - project-path -> ci-project-path
> - project-url -> ci-project-url
> - commit-sha -> ci-commit-sha
> - commit-tag -> ci-commit-tag
> - commit-ref-name -> ci-commit-ref-name
>

