---
title: "Get started"
anchor: "get-started"
description: The steps set up go-semrel-gitlab
weight: 30
---

# Getting started

1. Start [formatting commit messages](../commit-message) so that `go-semrel-gitlab` understands them
   - first line of form: `type(scope): subject`
   - breaking change marked with: `BREAKING CHANGE:`
1. Give `go-semrel-gitlab` permission to create tags and commit in your project
  - Create a [personal access token](https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html) with api scope
  - Assign it to GL_TOKEN in your project 'Settings → CI/CD → Variables'
1. add release tasks to your `.gitlab-ci.yml`
  - [example with binary](https://gitlab.com/juhani/go-semrel-gitlab/blob/8a0163b6bed3d44de3a5804d6ea4a234e2ac79ac/.gitlab-ci.yml#L81-83)
  - [example with docker image](https://gitlab.com/juhani/go-semrel-gitlab/blob/d3c3e8a7c8510f6fa83f5a41d9370ee81df40c84/.gitlab-ci.yml#L29-38)
