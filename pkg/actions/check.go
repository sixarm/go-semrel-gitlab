package actions

import (
	gitlab "github.com/xanzy/go-gitlab"
	"gitlab.com/juhani/go-semrel-gitlab/pkg/workflow"
)

// Check action tests api connection
type Check struct {
	client *gitlab.Client
}

// Do implements Action for Check
func (action *Check) Do() *workflow.ActionError {

	_, resp, err := action.client.Users.CurrentUser()
	if err != nil {
		retry := false
		if resp != nil && resp.StatusCode == 502 {
			retry = true
		}
		return workflow.NewActionError(err, retry)
	}
	return nil
}

// Undo implements Action for Check
func (action *Check) Undo() error {
	return nil
}

// NewCheck creates a Check action
func NewCheck(client *gitlab.Client) *Check {
	return &Check{
		client: client,
	}
}
